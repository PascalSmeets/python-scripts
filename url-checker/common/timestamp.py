from datetime import datetime

def ts():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]

def getDatetime(date_time_str):
    return  datetime.strptime(date_time_str, "%Y-%m-%d %H:%M:%S.%f")

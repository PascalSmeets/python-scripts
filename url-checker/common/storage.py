import csv
import os
import shutil

from common import filehandler, timestamp

currentpath = 'resources/sites/current/'
previouspath = 'resources/sites/previous/'
textextension = '.txt'
info = ' [INFO] '
warn = ' [WARN] '
error = ' [ERROR] '


def do_pre_run_checks(items_to_check_file, reportfile):
    # is there a dictionary named items_to_check.json present at the same level of this script
    input_file_exists = os.path.exists(items_to_check_file)
    if not input_file_exists:
        print(timestamp.ts() + " ERROR: MISSING INPUT: a required input file does not exist")
        f = filehandler.open_for_write(items_to_check_file)
        f.write("name,url,lastChecked")
        f.write("psek,https://psek.nl,2022-04-01 12:00:00.000")
        print(
            timestamp.ts() + info + "the application has created the file: '" + items_to_check_file + "' with a default site configured")

    # provision the report.txt. file
    report_file_exists = os.path.exists(reportfile)
    if not report_file_exists:
        open(reportfile, 'a').close()


def import_csv_as_sitelist(csvfile):
    sites_list = []
    with open(csvfile, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        next(reader)  # skip the header
        for row in reader:
            from common.classes import checkedSite
            sites_list.append(checkedSite(row[0], row[1], row[2], row[3], row[4], row[5]))
    return sites_list


def save_updated_sitelist_to_csv(sites_list, csvfile):
    with open(csvfile, 'w', newline='') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(["name", "url", "currentChecked", "currentHasChanged", "previousChecked", "previousHasChanged"])
        for checked_site in sites_list:
            writer.writerow(
                [checked_site.name, checked_site.url, checked_site.currentChecked, checked_site.currentHasChanged,
                 checked_site.previousChecked, checked_site.previousHasChanged])


def save_contents_to_file(name, data):
    with filehandler.open_for_write(currentpath + name + textextension) as f:
        f.write(data)
        f.close()


def update_previous_data_with_current_data_for_changed_site(name):
    shutil.copy(currentpath + name + textextension, previouspath + name + textextension)
    print(
        timestamp.ts() + info + "Updated the previous version of the source for " + name + " with the current version")

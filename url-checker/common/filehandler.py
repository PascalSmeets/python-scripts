import os
import os.path
import ast

def import_json_file_as_dict(filename):

    # reading the data from the file
    with open(filename) as f:
        data = f.read()

    # print("Data type before reconstruction : ", type(data))
    # reconstructing the data as a dictionary
    dictionary = ast.literal_eval(data)
    # print("Data type after reconstruction : ", type(d))
    return dictionary

# Taken from https://stackoverflow.com/a/23794010/17238312
def open_for_write(path):
    """
    Open "path" for writing, creating any parent directories as needed.
    If the path is only a file skip the os.makedirs command
    """
    if '/' in path:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    return open(path, 'w', encoding='utf-8')


def open_for_append(path):
    """
    Open "path" for writing, creating any parent directories as needed.
    If the path is only a file skip the os.makedirs command
    """
    if '/' in path:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    return open(path, 'a', encoding='utf-8')

def prepend_line_to_file(path, line):
    """
    Open "path" for writing, creating any parent directories as needed.
    Move the cursor to the beginning and add a new line
    If the path is only a file skip the os.makedirs command
    """
    if '/' in path:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'r+', encoding='utf-8') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)


def open_for_read(path):
    """
       Open "path" for reading.
       If the path is only a file skip the os.makedirs command
       """
    if '/' in path:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    return open(path, 'r', encoding='utf-8')
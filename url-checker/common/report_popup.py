from tkinter import *

# reportlist = {
#     "cucumber": "https://raw.githubusercontent.com/cucumber/cucumber-jvm/main/CHANGELOG.md",
#     "restassured": "https://raw.githubusercontent.com/rest-assured/rest-assured/master/changelog.txt",
#     "psek": "https://psek.nl"
# }


bgcolor = "#92e8e3"
bgcolor_list = "lightgrey"
bgcolor_while = "white"
textcolor_header = "#5a2cbc"
root = Tk()


def open_link(link):
    print("clicked on a button for link " + link)


def close_app():
    root.destroy()


def show_report(reportlist, reportFile):
    # root = Tk()
    root.title('Report on changed sites')
    root.config(background=bgcolor)

    # layout all of the main containers
    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)

    # define the Frames
    top_frame = Frame(root, background=bgcolor)
    middle_frame = Frame(root, background=bgcolor)
    bottom_frame = Frame(root, background=bgcolor)

    # configure the Frames
    top_frame.grid_rowconfigure(0, weight=1)
    top_frame.grid_columnconfigure(1, weight=1)
    middle_frame.grid_rowconfigure(0, weight=1)
    middle_frame.grid_columnconfigure(1, weight=1)
    bottom_frame.grid_rowconfigure(1, weight=1)
    bottom_frame.grid_columnconfigure(1, weight=1)

    # place the Frames
    top_frame.grid(column=0, row=0, padx=10, pady=10, columnspan=5, sticky='nsew')
    middle_frame.grid(column=0, row=1, padx=10, pady=10, columnspan=5, sticky='nsew')
    bottom_frame.grid(column=0, row=2, padx=10, pady=10, columnspan=5, sticky='nsew')

    # fill top frame
    # create labels for the top row
    name_label = Label(top_frame, text="Name")
    link_label = Label(top_frame, text="This site has changed since the last check")
    # visit_label = Label(top_frame, text="Go")

    name_label.configure(foreground=textcolor_header, background=bgcolor, font=('Helvatical bold', 14))
    link_label.configure(foreground=textcolor_header, background=bgcolor, font=('Helvatical bold', 14))
    # visit_label.configure(foreground=textcolor_header, background=bgcolor, font=('Helvatical bold', 14))

    # place the labels for the top row
    name_label.grid(column=0, row=0, padx=10, pady=10, sticky=W)
    link_label.grid(column=1, row=0, columnspan=5, padx=10, pady=10, sticky=W)
    # visit_label.grid(column=5, row=0, padx=10, pady=10, sticky=W)

    # fill bottom frame

    # create elements
    button_close = Button(bottom_frame, text="Close", command=close_app)
    button_close.configure(background=bgcolor_list, padx=20, pady=20, )
    report_label = Label(bottom_frame, text="Complete report file (most recent messages on top)")

    reportText = open(reportFile).read()

    textarea_report = Text(bottom_frame)
    scrollbar_textarea_report = Scrollbar(bottom_frame, orient=VERTICAL, command=textarea_report.yview)
    scrollbar_textarea_report.config(command=textarea_report.yview)

    textarea_report.configure(width=200, yscrollcommand=scrollbar_textarea_report.set)
    textarea_report.insert('1.0', str(reportText))

    # place elements
    report_label.grid(column=0, row=98, sticky='nsew')
    button_close.grid(column=11, row=99, sticky='se')
    textarea_report.grid(column=0, row=99, columnspan=10, sticky=W)
    scrollbar_textarea_report.grid(column=9, row=99, sticky='ns')




    # create the rows from the reportlist, the list is supplied for now
    for index, (key, value) in enumerate(reportlist.items(), start=1):
        idx = str(index)
        # create the labels, entries and buttons
        result_label = Label(middle_frame, text=key)
        result_label.configure(background=bgcolor)

        result_link = Entry(middle_frame)
        result_link.configure(background=bgcolor_while, width=60)
        result_link.insert(0, value)

        # result_button = Button(middle_frame, text="Visit site", command=lambda: open_link(result_link.get()))
        # result_button.configure(background=bgcolor_list)

        # place the labels, entries and buttons
        result_label.grid(column=0, row=index, padx=10, pady=10, sticky=W)
        result_link.grid(column=1, row=index, columnspan=4, padx=10, pady=10, sticky=W)
        # result_button.grid(column=6, row=index, padx=5, pady=4, sticky='nesw')

    root.mainloop()


def show_report_based_on_changedlist(site_list, reportFile):
    # root = Tk()
    root.title('Report on changed sites')
    root.config(background=bgcolor)

    # layout all of the main containers
    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)

    # define the Frames
    top_frame = Frame(root, background=bgcolor)
    middle_frame = Frame(root, background=bgcolor)
    bottom_frame = Frame(root, background=bgcolor)

    # configure the Frames
    top_frame.grid_rowconfigure(0, weight=1)
    top_frame.grid_columnconfigure(1, weight=1)
    middle_frame.grid_rowconfigure(0, weight=1)
    middle_frame.grid_columnconfigure(1, weight=1)
    bottom_frame.grid_rowconfigure(1, weight=1)
    bottom_frame.grid_columnconfigure(1, weight=1)

    # place the Frames
    top_frame.grid(column=0, row=0, padx=10, pady=10, columnspan=5, sticky='nsew')
    middle_frame.grid(column=0, row=1, padx=10, pady=10, columnspan=5, sticky='nsew')
    bottom_frame.grid(column=0, row=2, padx=10, pady=10, columnspan=5, sticky='nsew')

    # fill top frame
    # create labels for the top row
    name_label = Label(top_frame, text="Name")
    link_label = Label(top_frame, text="This site has changed since the last check")
    # visit_label = Label(top_frame, text="Go")

    name_label.configure(foreground=textcolor_header, background=bgcolor, font=('Helvatical bold', 14))
    link_label.configure(foreground=textcolor_header, background=bgcolor, font=('Helvatical bold', 14))
    # visit_label.configure(foreground=textcolor_header, background=bgcolor, font=('Helvatical bold', 14))

    # place the labels for the top row
    name_label.grid(column=0, row=0, padx=10, pady=10, sticky=W)
    link_label.grid(column=1, row=0, columnspan=5, padx=10, pady=10, sticky=W)
    # visit_label.grid(column=5, row=0, padx=10, pady=10, sticky=W)

    # fill bottom frame

    # create elements
    button_close = Button(bottom_frame, text="Close", command=close_app)
    button_close.configure(background=bgcolor_list, padx=20, pady=20, )
    report_label = Label(bottom_frame, text="Complete report file (most recent messages on top)")

    reportText = open(reportFile).read()

    textarea_report = Text(bottom_frame)
    scrollbar_textarea_report = Scrollbar(bottom_frame, orient=VERTICAL, command=textarea_report.yview)
    scrollbar_textarea_report.config(command=textarea_report.yview)

    textarea_report.configure(width=200, yscrollcommand=scrollbar_textarea_report.set)
    textarea_report.insert('1.0', str(reportText))

    # place elements
    report_label.grid(column=0, row=98, sticky='nsew')
    button_close.grid(column=11, row=99, sticky='se')
    textarea_report.grid(column=0, row=99, columnspan=10, sticky=W)
    scrollbar_textarea_report.grid(column=9, row=99, sticky='ns')




    # create the rows from the reportlist, the list is supplied for now
    # for index, (key, value) in enumerate(reportlist.items(), start=1):

    for index, site in enumerate(site_list):
        idx = str(index)
        # create the labels, entries and buttons
        result_label = Label(middle_frame, text=site.name)
        result_label.configure(background=bgcolor)

        result_link = Entry(middle_frame)
        result_link.configure(background=bgcolor_while, width=60)
        result_link.insert(0, site.url)

        # result_button = Button(middle_frame, text="Visit site", command=lambda: open_link(result_link.get()))
        # result_button.configure(background=bgcolor_list)

        # place the labels, entries and buttons
        result_label.grid(column=0, row=index, padx=10, pady=10, sticky=W)
        result_link.grid(column=1, row=index, columnspan=4, padx=10, pady=10, sticky=W)
        # result_button.grid(column=6, row=index, padx=5, pady=4, sticky='nesw')

    root.mainloop()

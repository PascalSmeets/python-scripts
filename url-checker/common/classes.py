import distutils
from datetime import datetime

from common import timestamp


class checkedSite:
    name: str
    url: str
    currentChecked: datetime
    currentHasChanged: bool
    previousChecked: datetime
    previousHasChanged: bool

    def __init__(self, var1, var2, var3, var4, var5, var6):
        self.name = var1.strip()
        self.url = var2.strip()

        if '' == var3:
            self.currentChecked = ''
        else:
            self.currentChecked = timestamp.getDatetime(var3.strip())
        self.currentHasChanged = bool(distutils.util.strtobool(var4.strip()))

        if '' == var5:
            self.previousChecked = ''
        else:
            self.previousChecked = timestamp.getDatetime(var5.strip())
        self.previousHasChanged = bool(distutils.util.strtobool(var6.strip()))

#!/usr/bin/env python3

import filecmp
import requests

from common import filehandler, storage
from common import report_popup
from common import timestamp


reportfile = 'changed_sites.log'
currentpath = 'resources/sites/current/'
previouspath = 'resources/sites/previous/'
textextension = '.txt'
items_to_check_file = 'items_to_check.csv'
info = ' [INFO] '
warn = ' [WARN] '
error = ' [ERROR] '




def get_website_contents(checkedSite):
    try:
        r = requests.get(checkedSite.url, timeout=10.0)
    except requests.ConnectionError as e:
        print("OOPS!! Connection Error. Make sure you are connected to Internet. Technical Details given below.\n")
        print(str(e))
        return 'error retrieving website contents'
    except requests.Timeout as e:
        print("OOPS!! Timeout Error")
        print(str(e))
        return 'error retrieving website contents'
    except requests.RequestException as e:
        print("OOPS!! General Error")
        print(str(e))
        return 'error retrieving website contents'
    except KeyboardInterrupt:
        print("Someone closed the program")
        return 'error retrieving website contents'

    if (r.ok):
        return r.text
    else:
        return 'error retrieving website contents, http status = ' + r.status_code


def compare_current_with_previous(checkedSite):
    current = currentpath + checkedSite.name + textextension
    previous = previouspath + checkedSite.name + textextension

    # shallow comparison
    hasChanged = not filecmp.cmp(current, previous)

    print(timestamp.ts() + info + "COMPARISON: site has changed = " + str(
        hasChanged) + "; '" + checkedSite.name + "' with site '" + checkedSite.url + "'")

    if hasChanged:
        log_site_updated_message(checkedSite.name, checkedSite.url)

    return hasChanged


def log_site_updated_message(name, url):
    line = timestamp.ts() + warn + 'SITE UPDATED: ' + name + ': ' + url + '\n'
    filehandler.prepend_line_to_file(reportfile, line)


def store_current_site_data_as_previous_if_previous_does_not_exits(checkedSite):
    import os
    if not os.path.exists(previouspath + checkedSite.name + textextension):
        line = timestamp.ts() + info + "SITE NEW: this is the first time '" + checkedSite.name + ': ' + checkedSite.url + "' has been requested, storing current as previous"
        print(line)
        filehandler.prepend_line_to_file(reportfile, line)
        data = get_website_contents(checkedSite)
        with filehandler.open_for_write(previouspath + checkedSite.name + textextension) as f:
            f.write(data)


def store_current_site_data_as_current(checkedSite):
    data = get_website_contents(checkedSite)
    storage.save_contents_to_file(checkedSite.name, data)


def main():
    storage.do_pre_run_checks(items_to_check_file, reportfile)

    sites_list = storage.import_csv_as_sitelist(items_to_check_file)

    for checkedSite in sites_list:
        store_current_site_data_as_previous_if_previous_does_not_exits(checkedSite)

        checkedSite.previousChecked = checkedSite.currentChecked  # store current timestamp in previous, overwriting the old value
        checkedSite.previousHasChanged = checkedSite.currentHasChanged  # store current check result boolean in previous, overwriting the old value
        checkedSite.currentChecked = timestamp.ts()  # generate a new current timestamp

        store_current_site_data_as_current(checkedSite)

        data = get_website_contents(checkedSite)
        storage.save_contents_to_file(checkedSite.name, data)
        checkedSite.currentHasChanged = compare_current_with_previous(checkedSite)

    storage.save_updated_sitelist_to_csv(sites_list, items_to_check_file)

    changed_sites = [site for site in sites_list if site.currentHasChanged]

    for checkedSite in changed_sites:
        storage.update_previous_data_with_current_data_for_changed_site(checkedSite.name)
        print(timestamp.ts() + warn + 'CHANGES FOUND')

    if (len(changed_sites) == 0):
        print(timestamp.ts() + info + 'No changes detected in the targeted sites')

    if (len(changed_sites) > 0):
        report_popup.show_report_based_on_changedlist(changed_sites, reportfile)


main()

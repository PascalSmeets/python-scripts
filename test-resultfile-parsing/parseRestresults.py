import argparse
import json
import os
import re
import sys
from datetime import datetime


class MyTestResult:
    application: str
    repository: str
    environment: str
    date: datetime
    totalTests: int
    totalFailed: int
    totalErrors: int
    totalSkipped: int

    def __init__(self, application, repository, environment, date):
        self.application = application
        self.repository = repository
        self.environment = environment
        self.date = date

    def setTotalTests(self, input):
        self.totalTests = input

    def setTotalFailed(self, input):
        self.totalFailed = input

    def setTotalErrors(self, input):
        self.totalErrors = input

    def setTotalSkipped(self, input):
        self.totalSkipped = input

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=2)


def parse_command_line_input():
    # Parse command line arguments to a dictionary
    parser = argparse.ArgumentParser(description='Parsing a testresult to JSON')
    parser.add_argument('-n', '--name', help='The application name, i.e. "app-name"')
    parser.add_argument('-u', '--url', help='The application url, i.e. "https://app.my.domain/service/v1/"')
    parser.add_argument('-d', '--date',
                        help='The timestamp of the test, i.e. "2022-04-06 15:30:34.893 +02:00" or "2022-04-06 15:30", or "2022-04-06"')
    parser.add_argument('-e', '--env', help='The environment the test has run on, i.e. "acp", "tst"')
    parser.add_argument('-f', '--file',
                        help='The testresults file to parse, i.e. "failsafe-reports/failsafe-summary.xml"')
    parser.add_argument('-t', '--type', help='The type of the testresults file, supported are "failsafe","surefire" and "junit"')
    command_line_variables = vars(parser.parse_args())

    # Check if all the required inputs are supplied, exit if there are empty values
    keys_with_empty_values_list = [key for key, val in command_line_variables.items() if not val]
    if len(keys_with_empty_values_list) > 0:
        print('Please supply all arguments, use the -h option for details.')
        print('Values for these input aruments are missing' + str(keys_with_empty_values_list))
        print('Exiting program')
        sys.exit(1)

    # Log the settings to the command line
    # print(80 * '=')
    # print('Running the parser with settings: ')
    # print(command_line_variables)
    # print(80 * '=')

    return command_line_variables


def get_mytestresult_for(application, repository, date, environment):
    return MyTestResult(application, repository, date, environment)


def load_results_file_to_string(file):
    with open(file, 'r') as file:
        data = file.read().replace('\n', '')
        return data


def parse_resultfile(MyTestResult, command_line_variables, test_result_data, test_result_file_type):
    # Parse the file based on the supplied and supported type
    type = command_line_variables.get('type')
    if 'failsafe' == type:
        if '.xml' == test_result_file_type:
            MyTestResult = add_failsafe_xml_results(MyTestResult, test_result_data)
            return MyTestResult
    elif 'surefire' == type:
        if '.txt' == test_result_file_type:
            MyTestResult = add_surefire_txt_results(MyTestResult, test_result_data)
            return MyTestResult
        elif '.xml' == test_result_file_type:
            MyTestResult = add_surefire_xml_results(MyTestResult, test_result_data)
            return MyTestResult
    elif 'junit' == type:
        if '.xml' == test_result_file_type:
            MyTestResult = add_junit_results(MyTestResult, test_result_data)
            return MyTestResult
    else:
        print('Sorry no results could be parsed: type "' + type + '" is not supported')
        sys.exit(1)

# Parsed format: failsafe
# <completed>12</completed>
# <errors>12</errors>
# <failures>0</failures>
# <skipped>0</skipped>
def add_failsafe_xml_results(MyTestResult, data):
    totalTests = re.search('<completed>(.+)</completed>', data).group(1)
    totalFailed = re.search('<failures>(.+)</failures>', data).group(1)
    totalErrors = re.search('<errors>(.+)</errors>', data).group(1)
    totalSkipped = re.search('<skipped>(.+)</skipped>', data).group(1)

    MyTestResult.setTotalTests(totalTests)
    MyTestResult.setTotalFailed(totalFailed)
    MyTestResult.setTotalErrors(totalErrors)
    MyTestResult.setTotalSkipped(totalSkipped)

    return MyTestResult

# Parsed format: surefire xml
# <testsuite tests="48" failures="2" name="cucumber.RunTest" time="24.745" errors="0" skipped="0">
def add_surefire_xml_results(MyTestResult, data):
    totalTests = re.search('tests=\"(.+?)\"', data).group(1)
    totalFailed = re.search('failures=\"(.+?)\"', data).group(1)
    totalErrors = re.search('errors=\"(.+?)\"', data).group(1)
    totalSkipped = re.search('skipped=\"(.+?)\"', data).group(1)

    MyTestResult.setTotalTests(totalTests)
    MyTestResult.setTotalFailed(totalFailed)
    MyTestResult.setTotalErrors(totalErrors)
    MyTestResult.setTotalSkipped(totalSkipped)

    return MyTestResult

# Parsed format: surefire txt
# Tests run: 4, Failures: 1, Errors: 0, Skipped: 0, Time elapsed: 31.282 sec <<< FAILURE! - in nl.kvk.fid.fitnesse.debug.regressieTest
def add_surefire_txt_results(MyTestResult, data):
    totalTests = re.search('Tests run: (.+?),', data).group(1)
    totalFailed = re.search('Failures: (.+?),', data).group(1)
    totalErrors = re.search('Errors: (.+?),', data).group(1)
    totalSkipped = re.search('Skipped: (.+?),', data).group(1)

    MyTestResult.setTotalTests(totalTests)
    MyTestResult.setTotalFailed(totalFailed)
    MyTestResult.setTotalErrors(totalErrors)
    MyTestResult.setTotalSkipped(totalSkipped)

    return MyTestResult

# Parsed format: junit
# <?xml version="1.0" encoding="UTF-8"?><testsuite errors="0" failures="14" name="io.cucumber.core.plugin.JUnitFormatter" skipped="0" tests="14" time="927.498">
def add_junit_results(MyTestResult, data):
    totalTests = re.search('tests=\"(.+?)\"', data).group(1)
    totalFailed = re.search('failures=\"(.+?)\"', data).group(1)
    totalErrors = re.search('errors=\"(.+?)\"', data).group(1)
    totalSkipped = re.search('skipped=\"(.+?)\"', data).group(1)

    MyTestResult.setTotalTests(totalTests)
    MyTestResult.setTotalFailed(totalFailed)
    MyTestResult.setTotalErrors(totalErrors)
    MyTestResult.setTotalSkipped(totalSkipped)

    return MyTestResult


def main():
    command_line_variables = parse_command_line_input()

    # Generate MyTestResult object without results
    testresult = get_mytestresult_for(command_line_variables.get('name'),
                                      command_line_variables.get('url'),
                                      command_line_variables.get('env'),
                                      command_line_variables.get('date'))
    # Load the resultfile into a string
    test_result_data = load_results_file_to_string(command_line_variables.get('file'))
    filename, extension = os.path.splitext(command_line_variables.get('file'))

    # Parse the resultfile and return a complete MyTestResult object
    testresult = parse_resultfile(testresult, command_line_variables, test_result_data, extension)

    print(testresult.toJSON())

if __name__ == '__main__':
    main()

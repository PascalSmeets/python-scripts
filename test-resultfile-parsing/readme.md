# Demo of testresult file parsing to json

This script uses plain Python to parse different inputs to a single json outpur format for reporting
An example of the output is:
```json
{
  "application": "test-app",
  "date": "2022-04-19 13:00:00.000 +02:00",
  "environment": "acc",
  "repository": "https://gitlab.com/PascalSmeets/python-scripts",
  "totalErrors": "12",
  "totalFailed": "0",
  "totalSkipped": "0",
  "totalTests": "12"
}

```

Get help using the `-h` argument

```commandline
usage: parseRestresults.py [-h] [-n NAME] [-u URL] [-d DATE] [-e ENV] [-f FILE] [-t TYPE]

Parsing a testresult to JSON

optional arguments:
  -h, --help            show this help message and exit
  -n NAME, --name NAME  The application name, i.e. "app-name"
  -u URL, --url URL     The application url, i.e. "https://app.my.domain/service/v1/"
  -d DATE, --date DATE  The timestamp of the test, i.e. "2022-04-06 15:30:34.893 +02:00" or "2022-04-06 15:30", or "2022-04-06"
  -e ENV, --env ENV     The environment the test has run on, i.e. "acp", "tst"
  -f FILE, --file FILE  The testresults file to parse, i.e. "failsafe-reports/failsafe-summary.xml"
  -t TYPE, --type TYPE  The type of the testresults file, supported are "failsafe","surefire" and "junit"

```

### Usage examples surefire

#### txt
```commandline
python3 parseRestresults.py -n test-app -u https://gitlab.com/PascalSmeets/python-scripts -d '2022-04-19' -e acc -f 'resultfiles/surefire/cucumber.RunTest.txt' -t surefire 
```
```commandline
python3 parseRestresults.py -n test-app -u https://gitlab.com/PascalSmeets/python-scripts -d '2022-04-19 13:00' -e acc -f 'resultfiles/surefire/nl.fitnesse.debug.regressieTest.txt' -t surefire 
```

#### xml
```commandline
python3 parseRestresults.py -n test-app -u https://gitlab.com/PascalSmeets/python-scripts -d '2022-04-19 13:00:00.000' -e acc -f 'resultfiles/surefire/TEST-cucumber.RunTest.xml' -t surefire 
```
```commandline
python3 parseRestresults.py -n test-app -u https://gitlab.com/PascalSmeets/python-scripts -d '2022-04-19 13:00:00.000 +02:00' -e acc -f 'resultfiles/surefire/TEST-nl.fitnesse.debug.regressieTest.xml' -t surefire 
```

### Usage example junit
```commandline
python3 parseRestresults.py -n test-app -u https://gitlab.com/PascalSmeets/python-scripts -d '2022-04-19 13:00:00.000 +02:00' -e acc -f 'resultfiles/junit/cucumber-junit.xml' -t junit
```
### Usage example failsafe
```commandline
python3 parseRestresults.py -n test-app -u https://gitlab.com/PascalSmeets/python-scripts -d '2022-04-19 13:00:00.000 +02:00' -e acc -f 'resultfiles/failsafe-report/failsafe-summary.xml' -t failsafe
```